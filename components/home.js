function displayMenOutWear() {

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    // let elem = document.querySelector('.container-tabs div p');
    // let style = getComputedStyle(elem);
    // style.borderBottom = "2px solid #000000";

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'grid';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'block';


    homeViews[0].getElementsByClassName("men-outerwear-img")[0].style.display = 'block';
    homeViews[0].getElementsByClassName('men-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-img')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-tshirts')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-button')[0].style.display = 'none';
    // document.getElementsByClassName("add-item-style")[0].style.display = 'none';

    document.getElementsByClassName("product-details")[0].style.display = 'none';

    document.getElementById('items-title').innerHTML = "Men's Outerwear";
    document.querySelector('#items-title').style.setProperty('font-weight', 'bold');

    let mensWears = document.querySelectorAll('.item-category div img');

    console.log(document.querySelectorAll('.item-category div img'));
    for (let i = 0; i < mensWears.length; i++) {
        mensWears[i].src = '../assets/home/tshirts/10-15068B.jpg';

    }

}

function displayLadiesOutWear() {

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'grid';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'block';


    homeViews[0].getElementsByClassName("men-outerwear-img")[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-img')[0].style.display = 'block';
    homeViews[0].getElementsByClassName('ladies-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-tshirts')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-button')[0].style.display = 'none';
    // document.getElementsByClassName("add-item-style")[0].style.display = 'none';

    document.getElementsByClassName("product-details")[0].style.display = 'none';

    document.getElementById('items-title').innerHTML = "Ladies Outerwear";
    document.querySelector('#items-title').style.setProperty('font-weight', 'bold');
    let ladiesWears = document.querySelectorAll('.item-category div img');

    console.log(document.querySelectorAll('.item-category div img'));
    for (let i = 0; i < ladiesWears.length; i++) {
        ladiesWears[i].src = '../assets/home/tshirts/10-24102B.jpg';

    }
}

function displayMenTshirts() {

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'grid';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'block';


    homeViews[0].getElementsByClassName("men-outerwear-img")[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-img')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-tshirts')[0].style.display = 'block';
    document.getElementsByClassName("men-tshirts")[0].style.display = 'block'
    document.getElementsByClassName("ladies-tshirts")[0].style.display = 'none'
    homeViews[0].getElementsByClassName('men-ladies-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-button')[0].style.display = 'none';
    // document.getElementsByClassName("add-item-style")[0].style.display = 'none';

    document.getElementsByClassName("product-details")[0].style.display = 'none';

    document.getElementById('items-title').innerHTML = "Men's T-shirts";
    let elem = document.querySelector('#items-title').style.setProperty('font-weight', 'bold');
    let mensTshirts = document.querySelectorAll('.item-category div img');

    for (let i = 0; i < mensTshirts.length; i++) {
        mensTshirts[i].src = '../assets/home/tshirts/10-13058B.jpg';
    }
}

function displayLadiesTshirts() {

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'grid';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'block';


    homeViews[0].getElementsByClassName("men-outerwear-img")[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-img')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('ladies-outerwear-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-tshirts')[0].style.display = 'block';
    document.getElementsByClassName("men-tshirts")[0].style.display = 'none'
    document.getElementsByClassName("ladies-tshirts")[0].style.display = 'block'
    homeViews[0].getElementsByClassName('men-ladies-text')[0].style.display = 'none';
    homeViews[0].getElementsByClassName('men-ladies-button')[0].style.display = 'none';
    // document.getElementsByClassName("add-item-style")[0].style.display = 'none';

    document.getElementsByClassName("product-details")[0].style.display = 'none';

    document.getElementById('items-title').innerHTML = "Ladies T-shirts";
    document.querySelector('#items-title').style.setProperty('font-weight', 'bold');

    let ladiesTshirts = document.querySelectorAll('.item-category div img');

    for (let i = 0; i < ladiesTshirts.length; i++) {
        ladiesTshirts[i].src = '../assets/home/tshirts/10-23173B.jpg';
    }
}

function displayProductView(id) {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'none';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'none';

    document.getElementsByClassName("product-details")[0].style.display = 'flex';

    let clothes = document.querySelector('#' + id + ' img').getAttribute('src');
    console.log(clothes);

    if (clothes == '../assets/home/tshirts/10-13058B.jpg') {
        console.log('a');

        document.querySelector('.item img').src = '../assets/home/tshirts/large/10-13058A.jpg';
    } else if (clothes == '../assets/home/tshirts/10-15068B.jpg') {
        console.log('b');

        document.querySelector('.item img').src = '../assets/home/tshirts/large/10-15068A.jpg';
    } else if (clothes == '../assets/home/tshirts/10-23173B.jpg') {
        console.log('c');

        document.querySelector('.item img').src = '../assets/home/tshirts/large/10-23173A.jpg';
    } else if (clothes == '../assets/home/tshirts/10-24102B.jpg') {
        console.log('d');

        document.querySelector('.item img').src = '../assets/home/tshirts/large/10-24102A.jpg';
    }
}

function cartView() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'none';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'none';
    document.getElementsByClassName("container-tabs")[0].style.display = 'none';
    document.getElementsByClassName("product-details")[0].style.display = 'none';
    document.getElementsByClassName("checkout")[0].style.display = 'none';
    document.getElementsByClassName("cart-view")[0].style.display = 'block';
}

function checkoutView() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    let elems = document.getElementsByClassName("item-category");
    elems[0].style.display = 'none';
    let homeViews = document.getElementsByClassName("home-view");
    homeViews[0].style.display = 'none';
    document.getElementsByClassName("container-tabs")[0].style.display = 'none';
    document.getElementsByClassName("product-details")[0].style.display = 'none';
    document.getElementsByClassName("checkout")[0].style.display = 'grid';
    document.getElementsByClassName("cart-view")[0].style.display = 'none';
}